
%define SYS_EXIT 60
%define SYS_READ 0
%define SYS_WRITE 1
%define TAB 0x9
%define LINE_BREAK 0xA
%define SPACE 0x20
%define DIGIT_0 0x30
%define DIGIT_9 0x39

section .text

global string_length
global print_newline
global print_string
global read_word
global string_equals
global exit


exit: 
    mov rax, SYS_EXIT
    syscall


string_length:
    mov rcx, 0
    .cycle:
    cmp byte[rdi+rcx], 0
    je  .end
    inc rcx
    jmp .cycle
    .end:
    mov rax, rcx
    ret

print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, SYS_WRITE
    mov rdi, 1
    syscall
    ret


print_char:
    push rdi
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    mov rax, SYS_WRITE
    syscall
    pop rdi
    ret




print_newline:
    mov rdi, LINE_BREAK	
    call print_char
    ret

print_uint:
    mov rax, rdi
    mov r8, 10
    push 0
    .splitting_by_digits:
    xor rdx, rdx
    div r8
    add rdx, DIGIT_0
    push rdx
    test rax, rax
    jnz .splitting_by_digits
    .print:
    pop rdi
    test rdi, rdi
    jz .exit
    call print_char
    jmp .print
    .exit:
    ret


print_int:
    test rdi, rdi
    js .neg
    call print_uint
    ret
    .neg:
    push rdi
    mov rdi, "-"
    call print_char
    pop rdi
    neg rdi
    call print_uint
    ret




string_equals:
    xor rcx, rcx
    .loop:
   	mov al, byte[rdi+rcx]
    	mov dl, byte[rsi+rcx]
    	cmp al, dl
    	jne .not_equal
   	inc rcx
    	cmp al, 0
    	je .equal
    	jmp .loop
.not_equal:
    mov rax, 0
    ret
     .equal:
    mov rax, 1
    ret

read_char:
    xor rdi, rdi
    mov rdx, 1
    push rax
    mov rsi, rsp
    mov rax, SYS_READ
    syscall
    xor rax, rax
    ret


read_word:
    push rdi
    push rsi
    mov r8, rdi
    mov r9, rsi
    xor r10, r10
    .cycle_before_letters:
    call read_char
    cmp rax, 0
    je .end
    cmp rax, SPACE
    je .cycle_before_letters
    cmp rax, TAB
    je .cycle_before_letters
    cmp rax, LINE_BREAK
    je .cycle_before_letters
    jmp .write
    .reading_cycle:
    call read_char
    cmp rax, 0
    je .end
    cmp rax, SPACE
    je .end
    cmp rax, TAB
    je .end
    cmp rax, LINE_BREAK
    je .end
    jmp .write
    .write:
    mov byte[r8+r10], al
    inc r10
    dec r9
    test r9, r9
    jnz .reading_cycle
    jmp .check_last_letter
    .check_last_letter:
    test rax, rax
    jnz .error
    jmp .end
    .error:
    mov rax, 0
    ret
    .end:
    mov byte[r8+r10], 0
    mov rax, r8
    dec rcx
    mov rdx, r10
    pop rsi
    pop rdi
    ret


parse_uint:
    xor rcx, rcx
    xor rdx, rdx
    xor rax, rax
    mov r8, 10
    .reading_symbols:
    mov dl, byte[rdi+rcx]
    cmp rdx, DIGIT_0
    jl .finish_reading
    cmp rdx, DIGIT_9
    jg .finish_reading
    sub rdx, DIGIT_0
    jmp .shift_and_plus
    .shift_and_plus:
    push rdx
    mul r8
    pop rdx
    add rax, rdx
    inc rcx
    jmp .reading_symbols
    .finish_reading:
    mov rdx, rcx
    ret 



parse_int:
    cmp byte[rdi], "-"
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret

string_copy:
    push rdi
    call string_length
    pop rdi
    cmp rax, rdx
    jg .fail
    .cycle:
    mov r8b, byte[rdi]
    mov byte[rsi], r8b
    inc rdi
    inc rsi
    test r8b, r8b
    jnz .cycle 
    ret
    .fail:
    mov rax, 0
    ret



