extern read_word
extern print_string
extern print_newline
extern string_length
extern find_word
extern exit
global _start

section .data
er1: db "The line entered is too long",0
er2: db "Your word isn't in the dictionary",0
 
section .bss
buffer:  resb 255

section .text
%include "colon.inc"
%include "words.inc"

_start:
    mov rdi, buffer ;адрес буфера
    mov rsi, 255
    call read_word ; считывает в буфер 
    test rax, rax ; 
    jz .error1 
    mov rsi, next
    call find_word
    test rax, rax ; нашел или нет?
    jz .error2 

    add rax, 8
    mov rdi, rax
    call string_length
    add rdi, rax
    inc rdi
    call print_string
    call exit

    .error1: 
    mov rdi, er1
    call print_error
    call exit

    .error2:
    mov rdi, er2
    call print_error
    call exit

    print_error:
    push rdi
    call string_length
    pop rsi ; указатель на строку
    mov rdx, rax ; длина 
    mov rax, 1 ; в rax  1 системный номер write
    mov rdi, 2 ; дескриптор stderr
    syscall
    ret
