global find_word
extern string_equals

section .text
; rdi адрес нуль-терминированной строки 
; rsi адрес элемента в словаре 

find_word:
    xor rax, rax
    .loop:
    test rsi, rsi ; логическое И 
    jz .end  ; if zf==0 goto end
    push rsi
    add rsi, 8  
    call string_equals 
    pop rsi

    test rax, rax 
    jnz .found ; if zf==1 goto .found ( в rax 0, если строки не равны)
    mov rsi, [rsi] ; адрес следующего элемента в словаре
    jmp .loop
    .found:
    mov rax, rsi
    .end:
    ret
