NASM = nasm
NASMFLAGS = -felf64 
LD=ld

%.o: %.asm
	$(NASM) $(NASMFLAGS) $@ $<

all: main
main: main.o lib.o dict.o
	$(LD) -o programm $+

.phony: clean
clean:
	rm -f *.o main

